package model;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Marc
 * 
 * Java Bean that represents a Contact.
 *
 *
 */
public class Contact {
	
	private String firstName;
	private String lastName;
	private String image;
	private String email;
	private ArrayList<PhoneNumber> phoneNumbersList;
	
	
	/**
	 * Constructor without image and email
	 * @param firstName
	 * @param lastName
	 * @param List of phonenumbers
	 */
	public Contact(final String firstName, final String lastName, PhoneNumber... phoneNumbers) {
		this.firstName = firstName;
		this.lastName = lastName;
		
		phoneNumbersList = new ArrayList<PhoneNumber>();
		
		for (PhoneNumber phoneNumber: phoneNumbers) {
			phoneNumbersList.add(phoneNumber);
		}
	}
	
	
	/**
	 * 
	 * Constructor including setting all the fields of Contact Class.
	 * @param firstName
	 * @param lastName
	 * @param image
	 * @param email
	 * @param phoneNumbers
	 */
	public Contact(final String firstName, final String lastName, final String image, final String email, 
			PhoneNumber... phoneNumbers) {
		this(firstName, lastName, phoneNumbers);
		this.image = image;
		this.email = email;
	}
	
	
	/* Getter and Setter */
	
	public String getFirstName() {
		return this.firstName;
	}
	
	
	public String getLastName() {
		return this.lastName;
	}

	
	public String getEmail() {
		return this.email;
	}
	
	
	public String getImage() {
		return this.image;
	}
	
	
	public ArrayList<PhoneNumber> getPhoneNumbersList() {
		return this.phoneNumbersList;
	}


}
